/*
 * Copyright 1993-2015 NVIDIA Corporation.  All rights reserved.
 *
 * Please refer to the NVIDIA end user license agreement (EULA) associated
 * with this source code for terms and conditions that govern your use of
 * this software. Any use, reproduction, disclosure, or distribution of
 * this software and related documentation outside the terms of the EULA
 * is strictly prohibited.
 *
 */
/* This sample queries the properties of the CUDA devices present in the system via CUDA Runtime API. */

// Shared Utilities (QA Testing)

// std::system includes
#include <memory>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string>

#include <cuda_runtime.h>
#include <opencv2/imgcodecs.hpp>
// #include <opencv2/highgui.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/stitching.hpp>
// #include <opencv2/core/utility.hpp>
#include <opencv2/core/utility.hpp>

// CUDA-C includes
#include <cuda.h>

// This function wraps the CUDA Driver API into a template function
////////////////////////////////////////////////////////////////////////////////
// Program main
////////////////////////////////////////////////////////////////////////////////

using namespace std;
using namespace cv;

cv::Stitcher::Mode mode = cv::Stitcher::PANORAMA;
// cv::Stitcher::Mode modeSCANS = cv::Stitcher::SCANS;
std::vector<cv::Mat> imgs;
std::vector<cv::Mat> imgs2;
// std::string result_name = "resultDJI.jpg";

int main(int argc, char **argv)
{
    // CUDA Drivers
    std::cout<<"Starting..."<<argv[0]<<std::endl<<std::endl;
    std::cout<<"CUDA Device Query (Runtime API) version (CUDART static linking)"<<std::endl<<std::endl;
    int deviceCount = 0;
    cudaError_t error_id = cudaGetDeviceCount(&deviceCount);
    
    if (error_id != cudaSuccess){
        std::cout<<"cudaGetDeviceCount returned "<< (int)error_id<< cudaGetErrorString(error_id)<<std::endl;
        std::cout<<"Result = FAIL"<<std::endl;
        exit(EXIT_FAILURE);
    }
    // This function call returns 0 if there are no CUDA capable devices.
    if (deviceCount == 0)
        std::cout<<"There are no available device(s) that support CUDA"<<std::endl;
    else
        std::cout<<"Detected "<< deviceCount<<" CUDA Capable device(s)"<<std::endl<<std::endl;
    int dev, driverVersion = 0, runtimeVersion = 0;
    for (dev = 0; dev < deviceCount; ++dev){
        cudaSetDevice(dev);
        cudaDeviceProp deviceProp;
        cudaGetDeviceProperties(&deviceProp, dev);
        std::cout<<"Device: "<<dev<<" "<<deviceProp.name<<std::endl;
        // Console log
        cudaDriverGetVersion(&driverVersion);
        cudaRuntimeGetVersion(&runtimeVersion);
        std::cout<<"CUDA Driver Version / Runtime Version          "<< driverVersion/1000
                << (driverVersion%100)/10<<runtimeVersion/1000<<(runtimeVersion%100)/10<<std::endl;
        std::cout<<"CUDA Capability Major / Minor version number:          "<< driverVersion/1000
                << deviceProp.major<<deviceProp.minor<<std::endl<<std::endl;
    }
    
    // Stitching de Imagenes
    std::cout<<"Stitching de Imagenes"<<std::endl<<std::endl;
    for (int i = 1; i < argc-10; ++i) { 
        cv::Mat img = cv::imread(argv[i]);
        std::cout<<argv[i]<<" Processing... "<<std::endl; 
        if (img.empty()) { 
            cout << "Can't read image '" << argv[i] << "'\n"; 
            return -1; 
        } 
        imgs.push_back(img); 
        if (imgs.size()==10){
            std::cout<<"Define object to store the stitched image "<<std::endl;
            cv::Mat pano; 
            std::cout<<"Command to stitch all the images present in the image array "<<std::endl;
            cv::Ptr<cv::Stitcher> stitcher = cv::Stitcher::create(mode, true); 
            std::cout<<"Create a Stitcher class object with mode panoroma "<<std::endl;
            cv::Stitcher::Status status = stitcher->stitch(imgs, pano);
            if (status != cv::Stitcher::OK) { 
                std::cout << "Can't stitch images"<<std::endl; 
                return -1; 
            } 
            std::cout<<"Store a new image stiched from the given  "<<std::endl;
            cv::Size size(1280,720);
            // cv::Size size(640,480);
            cv::resize(pano, pano, size);
            imgs2.push_back(pano);
            std::string ss = "resultfullPano" + std::to_string(i) + ".jpg ";
            cv::imwrite(ss, pano);
            std::cout<<"Results is written"<<std::endl;
            imgs.clear();
            std::cout<<ss<<" Processing is done..."<<std::endl;
        }
    }
    std::cout<<"Size of imgsStitching: "<<imgs2.size()<<std::endl;
    for (int i = 0; i < imgs2.size(); i++){
        cv::Size size(1280,720);
        cv::resize(imgs2[i], imgs2[i], size);
        std::cout<<"Size in imgsStitching: "<<imgs2[i].size()<<std::endl;
    }
    std::cout<<"Size of imgsStitching: "<<imgs2.size()<<std::endl;
    cv::Mat fullPano;
    cv::Ptr<cv::Stitcher> stitcher2 = cv::Stitcher::create(mode, true); 
    cv::Stitcher::Status status2 = stitcher2->stitch(imgsStitching, fullPano);
    // if (status2 != cv::Stitcher::OK) { 
    //     cout << "Can't stitch images\n"; 
    //     return -1; 
    // } 
    std::cout<<"Store a new image stiched from the given  "<<std::endl;
    cv::imwrite("resultfullPano.jpg", fullPano);
    std::cout<<"Results is written"<<std::endl; 
    return 0;
}